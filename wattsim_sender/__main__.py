#!/usr/bin/env python3
from getpass import getpass
from wattsim_sender import StationSender
from wattsim_sender.sender import DEFAULT_PORTS
import argparse
import logging


def main():
    parser = argparse.ArgumentParser(
        description='WattSim station data sender',
    )
    parser.add_argument(
        '--url',
        default='http://localhost:8000/',
        help='Backend URL',
    )
    parser.add_argument(
        '-u', '--username',
        help='Username to use to authenticate to the backend API.'
             'If left unspecified, will be asked interactively.',
    )
    parser.add_argument(
        '-p', '--password',
        help='Password to use to authenticate to the backend API.'
             'If left unspecified, will be asked interactively.',
    )
    parser.add_argument(
        '-v', '--verbose',
        action='store_const',
        const=logging.DEBUG,
        default=logging.INFO,
        dest='loglevel',
        help='Increase the logs verbosity. Useful for debugging.',
    )
    parser.add_argument(
        'ports',
        nargs='*',
        default=DEFAULT_PORTS,
        metavar='port',
        help='One or more ports to try to connect to.',
    )

    kwargs = vars(parser.parse_args())
    if not kwargs.get('username'):
        kwargs['username'] = input('Username: ')
    if not kwargs.get('password'):
        kwargs['password'] = getpass('Password: ')

    logging.basicConfig(
        format='[%(levelname)s/%(name)s] %(message)s',
        level=kwargs.pop('loglevel', logging.INFO),
    )

    StationSender(**kwargs).run()


if __name__ == '__main__':
    main()
