from urllib.parse import urljoin
import logging
import requests
import serial

logger = logging.getLogger(__name__)
DEFAULT_PORTS = ('/dev/ttyACM0', '/dev/ttyACM1', '/dev/ttyACM2')


class StationSender(object):

    def __init__(self,
                 url='http://localhost:8000/',
                 username=None,
                 password=None,
                 ports=DEFAULT_PORTS):
        self.base_url = url
        self.username = username
        self.password = password
        self.ports = ports

        self.session = requests.Session()
        self.session.headers['Referrer'] = self.base_url

    def authenticate(self):
        url = urljoin(self.base_url, '/api/v1/user/')
        logger.debug('Authenticating to {!r} with username {!r}'.format(
            url,
            self.username,
        ))

        resp = self.session.post(url, json={
            'username': self.username,
            'password': self.password,
        })
        resp.raise_for_status()

        logger.debug('Setting X-CSRFToken to {!r}'.format(
            self.session.cookies['csrftoken']))
        self.session.headers['X-CSRFToken'] = self.session.cookies['csrftoken']

    def build_payload(self, data):
        if data.count(',') != 2:
            return
        solar, wind, hydro = data.strip().split(',')
        payload = {
            "solar": solar,
            "wind": wind,
            "hydro": hydro
        }
        logger.debug('Built measurement payload: {!r}'.format(payload))
        return payload

    def send(self, data):
        url = urljoin(self.base_url, '/api/v1/measurements/')
        logger.debug('Uploading measurement to {!r}'.format(url))
        payload = self.build_payload(data)
        if not payload:
            return
        resp = self.session.post(url, json=payload)
        resp.raise_for_status()
        logger.debug('New measurement ID: {!r}'.format(resp.json()['id']))

    def open(self):
        for port in self.ports:
            if self._open_serial(port):
                return port
        raise OSError('No serial ports could be opened.')

    def _open_serial(self, port):
        logger.debug('Looking for serial port at {}'.format(port))
        try:
            self.serial = serial.Serial(
                port=port,
                baudrate=115200,
                parity=serial.PARITY_ODD,
                stopbits=serial.STOPBITS_TWO,
                bytesize=serial.SEVENBITS
            )
        except serial.SerialException as e:
            logger.debug('Could not open {}: {!s}'.format(port, e))
            return False
        return self.serial.isOpen()

    def read(self):
        return self.serial.readline().decode('utf-8')

    def run(self):
        logger.info('Opening serial port…')
        opened_port = self.open()
        logger.info('Using {}'.format(opened_port))
        logger.info('Authenticating…')
        self.authenticate()
        logger.info('Starting measurement uploads')
        while True:
            data = self.read()
            self.send(data)
